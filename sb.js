function sliceBy(arr, by){
    var r = [], L = Math.ceil(arr.length/by);
    if (by > 0 && arr.length > 0)
        for (var i = 0; i < L; i++)
            r.push(arr.slice(i * by, by + i * by));
    return r;
}